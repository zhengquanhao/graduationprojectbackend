var dbConfig = require("../util/dbconfig");

// 根据品牌获取的商品列表
addCartList = (req,res) => {
    let productId = (req.param('productId')).toString();
    let cookieId = (req.param('cookieId')).toString();
    let productName = req.param('productName')
    let productPrice = req.param('productPrice')
    let shopName = req.param('shopName')
    let img = req.param('img')
    let count = 1;

    console.log(productName)
    console.log(productPrice)
    console.log(shopName)
    console.log(img)

    // 判断是否表里已经有该商品,有的话只增加数量即可
    let sql = `select * from cart where productId=${productId} and userId=${cookieId}`
    let sqlArr = [];
    let callback = (err,data) => {
        if (err) {
            console.log("连接出错");
            return;
        } else {
            // data => 未过滤后的数据
            if (String(data) === String([])) {
                let sqlInsertCart = `insert into cart(userId,productId,productName,productPrice,shopName,img,count) values(${cookieId},${productId},${productName},${productPrice},${shopName},${img},${count})`                
                
                let sqlInsertCartArr = [];
                let callback = (err,data) => {
                    if (err) {
                        console.log("连接出错111");
                        return;
                    } else {
                        console.log(data);
                        res.send({
                            code:200,
                            success:true,
                            message:"添加成功"
                        })
                    }
                }
                dbConfig.sqlConnect(sqlInsertCart,sqlInsertCartArr,callback);
            } else {
                // 判断是否表里已经有该商品,有的话只增加数量即可
                let sql1 = `select count from cart where productId=${productId} and userId=${cookieId}`
                let sqlArr1 = [];
                let callback1 = (err,data) => {
                    if (err) {
                        console.log("连接出错");
                    return;
                    } else {
                        let sqlUpdateCart = `update cart set count=${data[0].count + 1} where productId=${productId} and userId=${cookieId}`        
                        let sqlUpdateCartArr = [];
                        let callbackUpdateCart = (err,updatedata) => {
                            if (err) {
                                console.log("连接出错");
                            return;
                            } else {
                                // cartdata => 更新后的数据
                                res.send({
                                    code:200,
                                    success:true,
                                    message:"添加成功",
                                    desc:"增加count数量"
                                })
                            }
                        }
                        dbConfig.sqlConnect(sqlUpdateCart,sqlUpdateCartArr,callbackUpdateCart);
                    }
                }
                dbConfig.sqlConnect(sql1,sqlArr1,callback1);            
            }
        }
    }
    dbConfig.sqlConnect(sql,sqlArr,callback);
    
}


// 根据账户获取购物车
getCartList = (req,res) => {
    let cookieId = req.param('cookieId')

    let sql = `select * from cart where userId=${cookieId}`

    let sqlArr = [];
    let callback = (err,data) => {
        if (err) {
            console.log("连接出错");
        return;
        } else {
            res.send(data);
        }
    }
    dbConfig.sqlConnect(sql,sqlArr,callback);
}

// 根据账户获取购物车
deleteCartList = (req,res) => {
    let cookieId = req.param('cookieId')
    let productId = req.param('productId')
    console.log(cookieId)
    console.log(productId)

    let sql = `select * from cart where userId=${cookieId} and productId=${productId}`

    let sqlArr = [];
    let callback = (err,data) => {
        if (err) {
            console.log("连接出错");
        return;
        } else {
            let sqlUpdateCart = `delete from cart where productId=${productId} and userId=${cookieId}`        
            let sqlUpdateCartArr = [];
            let callbackUpdateCart = (err) => {
                if (err) {
                    console.log("连接出错");
                return;
                } else {
                    // cartdata => 更新后的数据
                    res.send({
                        code:200,
                        success:true,
                        message:"删除成功",
                    })
                }
            }
            dbConfig.sqlConnect(sqlUpdateCart,sqlUpdateCartArr,callbackUpdateCart);
        }
    }
    dbConfig.sqlConnect(sql,sqlArr,callback);
}



module.exports = {
    addCartList,
    getCartList,
    deleteCartList
}