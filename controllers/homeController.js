var dbConfig = require("../util/dbconfig");

// 获取分类
getLytab = (req,res)=> {
    let sql = "select * from home_const_lytab";
    let sqlArr = [];
    let callback = (err,data) => {
        if (err) {
            console.log("连接出错");
        return;
        } else {
            // 返回数据
            res.send({
                "map":data
            })
        }
    }
    dbConfig.sqlConnect(sql,sqlArr,callback);
}

// 获取推荐模块轮播
getRecommendSwiper = (req,res)=> {
    let sql = "select * from home_recommend_swiper";
    let sqlArr = [];
    let callback = (err,data) => {
        if (err) {
            console.log("连接出错");
        return;
        } else {
            res.send({
                "map":data
            })
            
        }
    }
    dbConfig.sqlConnect(sql,sqlArr,callback);
}

// 获取推荐模块轮播
getInternationSwiper = (req,res)=> {
    let sql = "select * from home_internation_swiper";
    let sqlArr = [];
    let callback = (err,data) => {
        if (err) {
            console.log("连接出错");
        return;
        } else {
            res.send({
                "map":data
            })
            
        }
    }
    dbConfig.sqlConnect(sql,sqlArr,callback);
}

// 获取推荐模块分类数据
getRecommendCategory = (req,res)=> {
    let sql = "select * from home_recommend_category";
    let sqlArr = [];
    let callback = (err,data) => {
        if (err) {
            console.log("连接出错");
        return;
        } else {
            res.send({
                "map":data
            })
            
        }
    }
    dbConfig.sqlConnect(sql,sqlArr,callback);
}

// 获取推荐模块vip大牌日图片
getRecommendVip = (req,res)=> {
    let sql = "select * from home_recommend_vip";
    let sqlArr = [];
    let callback = (err,data) => {
        if (err) {
            console.log("连接出错");
        return;
        } else {
            res.send({
                "map":data
            })
            
        }
    }
    dbConfig.sqlConnect(sql,sqlArr,callback);
}

// 获取推荐模块今日特卖数据
getRecommendSpecialSales = (req,res)=> {
    let sql = "select * from home_recommend_special";
    let sqlArr = [];
    let callback = (err,data) => {
        if (err) {
            console.log("连接出错");
        return;
        } else {
            res.send({
                "map":data
            }) 
        }
    }
    dbConfig.sqlConnect(sql,sqlArr,callback);
}


module.exports = {
    getLytab,
    getRecommendSwiper,
    getInternationSwiper,
    getRecommendCategory,
    getRecommendVip,
    getRecommendSpecialSales
}