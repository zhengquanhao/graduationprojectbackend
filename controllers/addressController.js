var dbConfig = require("../util/dbconfig");

// 根据用户设置地址
setAddressById = (req,res) => {
    let cookieId = (req.param('cookieId')).toString();
    let name = req.param('name')
    let fullAddress = req.param('fullAddress')
    let tel = req.param('tel')
    let postalCode = req.param('postalCode')

    let sql = `select * from address where userId=${cookieId}`
    let sqlArr = [];
    let callback = (err,data) => {
        if (err) {
            console.log("查询连接出错");
            return;
        } else {
            if (String(data) === String([])) {
                let sqlInsertCart = `insert into address(userId,name,fullAddress,tel,postalCode) values(${cookieId},${name},${fullAddress},${tel},${postalCode})`                         
                let sqlInsertCartArr = [];
                let callback = (err,data) => {
                    if (err) {
                        console.log("地址连接出错");
                        return;
                    } else {
                        console.log(data);
                        res.send({
                            code:200,
                            success:true,
                            message:"添加成功"
                        })
                    }
                }
                dbConfig.sqlConnect(sqlInsertCart,sqlInsertCartArr,callback);
            } else {
                console.log(name);
                let updatename = (req.param('name')).replace(/\"/g, "")
                console.log(updatename);
                let sqlUpdateCart = `update address set name=${name},fullAddress=${fullAddress},tel=${tel},postalCode=${postalCode} where userId=${cookieId}`        
                let sqlUpdateCartArr = [];
                let callbackUpdateCart = (err,updatedata) => {
                    if (err) {
                        console.log("更新连接出错");
                    return;
                    } else {
                        // cartdata => 更新后的数据
                        res.send({
                            code:200,
                            success:true,
                            message:"更新成功"
                        })
                    }
                }
                dbConfig.sqlConnect(sqlUpdateCart,sqlUpdateCartArr,callbackUpdateCart);          
            }
        }
    }
    dbConfig.sqlConnect(sql,sqlArr,callback);
}
getAddressById = (req,res) => {
    let cookieId = req.param('cookieId')

    let sql = `select * from address where userId=${cookieId}`

    let sqlArr = [];
    let callback = (err,data) => {
        if (err) {
            console.log("连接出错");
        return;
        } else {
            res.send(data);
        }
    }
    dbConfig.sqlConnect(sql,sqlArr,callback);
}

// 根据用户获取地址
module.exports = {
    setAddressById,
    getAddressById
}