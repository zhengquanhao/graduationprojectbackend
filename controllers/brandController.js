var dbConfig = require("../util/dbconfig");

// 根据品牌获取的商品列表
getProductByBrandId = (req,res) => {
    let brand_id = req.param('brandId');
    // let sql = `select * from product where brand_id=${brand_id} and product_id=?`
    let sql = `select * from product where brand_id=${brand_id}`
    
    let sqlArr = [];
    let callback = (err,data) => {
        if (err) {
            console.log("连接出错");
        return;
        } else {
            // 返回数据
            res.send({
                "map":data
            })
        }
    }
    dbConfig.sqlConnect(sql,sqlArr,callback);
}

// 根据品牌获取的商品标题
getBrandTitle = (req,res) => {
    let brand_id = req.param('brandId');
    // let sql = `select * from product where brand_id=${brand_id} and product_id=?`
    let sql = `select * from brand_title where brand_id=${brand_id}`
    
    let sqlArr = [];
    let callback = (err,data) => {
        if (err) {
            console.log("连接出错");
        return;
        } else {
            // 返回数据
            res.send({
                "map":data
            })
        }
    }
    dbConfig.sqlConnect(sql,sqlArr,callback);
}

module.exports = {
    getProductByBrandId,
    getBrandTitle
}