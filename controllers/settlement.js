var dbConfig = require("../util/dbconfig");

// 添加订单
addOrderList = (req,res) => {
    let productId = (req.param('productId')).toString();
    let cookieId = (req.param('cookieId')).toString();
    let productName = req.param('productName')
    let productPrice = req.param('productPrice')
    let shopName = req.param('shopName')
    let img = req.param('img')
    let count = req.param('count') || 1;
    let orderType = "1";
    let sqlInsert = `insert into settlement(userId,productId,productName,productPrice,shopName,img,count,orderType) values(${cookieId},${productId},${productName},${productPrice},${shopName},${img},${count},${orderType})`                          
    let sqlInsertArr = [];
    let callback = (err,data) => {
        if (err) {
            console.log("连接出错");
            return;
        } else {
            res.send({
                code:200,
                success:true,
                message:"购买成功"
            })
        }
    }
    dbConfig.sqlConnect(sqlInsert,sqlInsertArr,callback);
}

// 获取订单列表
getOrderList = (req,res) => {
    let cookieId = req.param('cookieId')
    let type = req.param('type')
    console.log(cookieId,type)
    let sql;
    if (type) {
        sql = `select * from settlement where userId=${cookieId} and orderType=${type}`
    } else {
        sql = `select * from settlement where userId=${cookieId}`
    }
    let sqlArr = [];
    let callback = (err,data) => {
        if (err) {
            console.log("连接出错");
        return;
        } else {
            res.send(data);
        }
    }
    dbConfig.sqlConnect(sql,sqlArr,callback);
}

module.exports = {
    addOrderList,
    getOrderList
}