var dbConfig = require("../util/dbconfig");

// 根据品牌获取的商品列表
getDetailsById = (req,res) => {
    let productId = req.param('productId');
    // let sql = `select * from product where brand_id=${brand_id} and product_id=?`
    let sql = `select * from product_details where productId=${productId}`
    
    let sqlArr = [];
    let callback = (err,data) => {
        if (err) {
            console.log("连接出错");
        return;
        } else {
            data[0].detailsImgs = data[0].detailsImgs.split(',')
            data[0].swiperImgs = data[0].swiperImgs.split(',')

            // 返回数据
            res.send({
                "map":data
            })
        }
    }
    dbConfig.sqlConnect(sql,sqlArr,callback);
}
// /images/product/6918723241053264147/shopLogo.webp
// /images/product/6918723241053264147/swiper/1.webp,/images/product/6918723241053264147/swiper/2.webp,/images/product/6918723241053264147/swiper/3.webp,/images/product/6918723241053264147/swiper/4.webp
// /images/product/6918723241053264147/details/1.webp,/images/product/6918723241053264147/details/2.webp,/images/product/6918723241053264147/details/3.webp,/images/product/6918723241053264147/details/4.webp,/images/product/6918723241053264147/details/5.webp

module.exports = {
    getDetailsById
}