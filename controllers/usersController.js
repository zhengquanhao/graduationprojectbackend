var dbConfig = require("../util/dbconfig");

loginByPassword = (req,res) => {
    let uname = req.param('uname');
    let password = req.param('password');
    let sqlQuery = `select * from register_password where uname=${uname}`
    
    let sqlArr = [];
    let callback = (err,data) => {
        if (err) {
            console.log("连接出错");
            return;
        } else {
            console.log(data)
            // 判断数据库是否有对应值
            if (String(data) == String([])) {
                res.send({
                    code:200,
                    success:false,
                    message:"用户名未注册"
                });
                return;
            }
            
            data.some (item => {
                if (item.uname === uname.replace(/\"/g, "") && item.password === password.replace(/\"/g, "")) {
                    res.send({
                        code:200,
                        success:true,
                        message:"登录成功",
                        data:data
                    })
                } 
                if (item.uname === uname.replace(/\"/g, "") && item.password !== password.replace(/\"/g, "")) {
                    res.send({
                        code:200,
                        success:false,
                        message:"密码错误"
                    })
                }
            })
        }
    }
    dbConfig.sqlConnect(sqlQuery,sqlArr,callback);
}
// 通过密码注册
registerByPassword = (req,res) => {
    let uname = req.param('uname');
    let password = req.param('password');
    let sqlQuery = `select * from register_password where uname=${uname}`
    
    let sqlArr = [];
    let callback = (err,data) => {
        if (err) {
            console.log("连接出错");
            return;
        } else {
            // 去重（已注册的用户名无法重复注册）
            if (String(data) !== String([])) {
                res.send({
                    code:200,
                    success:false,
                    message:"该用户已被注册",
                    messageType:1
                })
                return;
            }
            // 未注册的用户名添加到数据库
            let sqlInsert = `insert into register_password(uname,password) values(${uname},${password})`
            let sqlInsertArr = [];
            let callback = (err,data) => {
                if (err) {
                    console.log("连接出错");
                return;
                } else {
                    res.send({
                        code:200,
                        success:true
                    })
                }
            }
            dbConfig.sqlConnect(sqlInsert,sqlInsertArr,callback);
        }
    }
    dbConfig.sqlConnect(sqlQuery,sqlArr,callback);
}

// 根据id获取用户账户信息
getUserInfoById = (req,res) => {
    let id = req.param('id');
    let sqlQuery = `select * from register_password where id=${id}`
    
    let sqlArr = [];
    let callback = (err,data) => {
        if (err) {
            console.log("连接出错");
            return;
        } else {
            res.send(data);
        }
    }
    dbConfig.sqlConnect(sqlQuery,sqlArr,callback);
}

module.exports = {
    loginByPassword,
    registerByPassword,
    getUserInfoById
}