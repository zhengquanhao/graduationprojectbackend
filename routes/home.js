var express = require('express');
var router = express.Router();

let home = require("../controllers/homeController")

router.get('/getLytab', home.getLytab);
router.get('/recommend/swiper', home.getRecommendSwiper);
router.get('/internation/swiper', home.getInternationSwiper);
router.get('/recommend/category', home.getRecommendCategory);
router.get('/recommend/vip', home.getRecommendVip);
router.get('/recommend/specialSales', home.getRecommendSpecialSales);


module.exports = router;
