var express = require('express');
var router = express.Router();

let cart = require("../controllers/cartController")

router.get('/getCartList', cart.getCartList);
router.get('/deleteCartList', cart.deleteCartList);
router.get('/addCartList', cart.addCartList);


module.exports = router;