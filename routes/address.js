
var express = require('express');
var router = express.Router();

let address = require("../controllers/addressController")

router.get('/setAddressById', address.setAddressById);
router.get('/getAddressById', address.getAddressById);

module.exports = router;