var express = require('express');
var router = express.Router();

let settlement = require("../controllers/settlement")

router.get('/addOrderList', settlement.addOrderList);
router.get('/getOrderList', settlement.getOrderList);


module.exports = router;