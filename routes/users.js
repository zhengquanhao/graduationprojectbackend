var express = require('express');

var router = express.Router();

let users = require("../controllers/usersController")

router.get('/loginByPassword', users.loginByPassword);
router.get('/registerByPassword', users.registerByPassword);
router.get('/getUserInfoById', users.getUserInfoById);



module.exports = router;


