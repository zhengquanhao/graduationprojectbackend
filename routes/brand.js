
var express = require('express');
var router = express.Router();

let brand = require("../controllers/brandController")

router.get('/getBrandList', brand.getProductByBrandId);
router.get('/getBrandTitle', brand.getBrandTitle);

module.exports = router;

// /images/product/6917917083053478547/details/1.webp,/images/product/6917917083053478547/details/2.webp,