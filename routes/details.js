var express = require('express');
var router = express.Router();

let details = require("../controllers/detailsController")

router.get('/getDetailsById', details.getDetailsById);

module.exports = router;