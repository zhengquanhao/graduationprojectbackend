const mysql = require('mysql');
module.exports = {
    // 数据库配置
    config: {
        host:"localhost",
        port:"3306",
        user:"root",
        password:"123456",
        database:"graduation"
    },
    // 连接数据库,使用mysql的连接池的连接方式(数据很大时会减少查询的时间)
    // 连接池对象
    sqlConnect: function(sql,sqlArr,callback) {
        let pool = mysql.createPool(this.config);
        // console.log(pool)
        pool.getConnection((err,coon) => {
            if (err) {
                console.log("连接失败");
                return;
            }
            // 事件驱动回调
            coon.query(sql,sqlArr,callback);
            // 释放连接
            coon.release();
        })
    }
}